package fr.tuetuopay.spawnreturn;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.util.Vector;

/**
 * Created by Alexis on 05/05/2014.
 */
public class SpawnReturn extends JavaPlugin implements Listener {
	public Vector pos1 = new Vector (0.0, 0.0, 0.0), pos2 = new Vector (0.0, 0.0, 0.0), spawn = new Vector (0.0, 0.0, 0.0);
	public boolean useServerSpawn = true;

	@Override
	public void onEnable () {
		saveDefaultConfig ();

		getServer().getPluginManager().registerEvents (this, this);

		loadConfing ();
	}
	@Override
	public void onDisable ()    {

	}

	@Override
	public boolean onCommand (CommandSender sender, Command cmd, String label, String[] args)   {
		if (cmd.getName ().equalsIgnoreCase ("sr")) {
			if (args.length >= 1)    {
				if (args[0].equalsIgnoreCase ("reload")) {
					if (sender instanceof Player) {
						if (((Player) sender).hasPermission ("reload")) {
							loadConfing ();
							((Player) sender).sendMessage ("" + ChatColor.ITALIC + "[SR] Loaded config.");
							return true;
						}
						((Player) sender).sendMessage ("" + ChatColor.RED + "Vous n'avez pas la permission de faire ceci.");
						return false;
					}
					loadConfing ();
					return true;
				}
				if (sender instanceof Player) {
					Player p = (Player)sender;
					if (!p.hasPermission ("edit"))
						return false;
					if (args[0].equalsIgnoreCase ("pos1")) {
						pos1 = p.getLocation ().toVector ();
						saveMyConfig ();
						reorderPositions ();
						p.sendMessage (""+ChatColor.GREEN+"[SR] Firt position set to "+pos1+".");
						return true;
					}
					if (args[0].equalsIgnoreCase ("pos2")) {
						pos2 = p.getLocation ().toVector ();
						saveMyConfig ();
						reorderPositions ();
						p.sendMessage (""+ChatColor.GREEN+"[SR] Second position set to "+pos2+".");
						return true;
					}
					if (args[0].equalsIgnoreCase ("spawn")) {
						spawn = p.getLocation ().toVector ();
						saveMyConfig ();
						p.sendMessage (""+ChatColor.GREEN+"[SR] Spawn position set to "+spawn+".");
						return true;
					}
					if (args[0].equalsIgnoreCase ("fill"))  {
						Location l = new Location (p.getWorld (), 0.0, 0.0, 0.0);
						for (int x = (int)pos1.getX (); x <= pos2.getX (); x++)
							for (int y = (int)pos1.getY (); y <= pos2.getY (); y++)
								for (int z = (int)pos1.getZ (); z <= pos2.getZ (); z++) {
									l.setX (x);
									l.setY (y);
									l.setZ (z);
									l.getBlock ().setType (Material.STONE);
								}
						return true;
					}
				}
			}
			return false;
		}
		return false;
	}

	private void reorderPositions ()    {
		double b = 0.0;
		if (pos1.getX () > pos2.getX ())    {
			b = pos1.getX ();
			pos1.setX (pos2.getX ());
			pos2.setX (b);
		}
		if (pos1.getY () > pos2.getY ())    {
			b = pos1.getY ();
			pos1.setY (pos2.getY ());
			pos2.setY (b);
		}
		if (pos1.getZ () > pos2.getZ ())    {
			b = pos1.getZ ();
			pos1.setZ (pos2.getZ ());
			pos2.setZ (b);
		}
	}
	private void loadConfing () {
		getLogger ().info ("Loaded config.");

		useServerSpawn = getConfig ().getBoolean ("spawn.server");
		pos1 = getConfig ().getVector ("region.min");
		pos2 = getConfig ().getVector ("region.max");
		spawn = getConfig ().getVector ("spawn.pos");

		reorderPositions ();
		saveMyConfig ();
	}
	private void saveMyConfig ()  {
		getConfig ().set ("region.min", pos1);
		getConfig ().set ("region.max", pos2);
		getConfig ().set ("spawn.pos", spawn);
		getConfig ().set ("spawn.server", useServerSpawn);
		saveConfig ();
	}

	@EventHandler
	public void onPlayerMove (PlayerMoveEvent event)    {
		Player p = event.getPlayer ();
		Location l = p.getLocation ();
		if (l.getX () >= pos1.getX () && l.getX () <= pos2.getX () &&
			l.getY () >= pos1.getY () && l.getY () <= pos2.getY () &&
			l.getZ () >= pos1.getZ () && l.getZ () <= pos2.getZ ())
			p.teleport ((useServerSpawn) ? p.getWorld ().getSpawnLocation () : p.getLocation ().zero ().add (spawn));
	}
}
