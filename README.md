# SpawnReturn #

Simple Bukkit plugin. You define a zone in the world, and any player entering inside this zone gets teleported to the World's spawn point or to a custom point. Useful for server lobbies made as islands.

You can see it in action on this server : [BlackCraft.fr](http://blackcraft.fr)

## Usage ##

There are a few InGame commands to know for this plugin :

* `/sr pos1` : defines the first corner of the area. This is the equivalent of the left-click with the WorldEdit wand
* `/sr pos2` : defines the second corner of the area. This is the equivalent of the right-click with the WorldEdit wand
* `/sr spawn` : defines the current position as the location where players get teleported *only if the `spawn.server` node has been set to false in the config file*
* `/sr reload` : reloads the configuration file from disk. Note that this will overwrite the changes you made via ingame commands.
* `/sr fille` : fills the whole selection with stone. WARNING : this is a "troubleshooting" tool meant for easy visualization of the area. There is NO optimization in this command, so expect it to make the server lag.

The only thing you cannot set via ingame commands is wether the return point is the world's spawn point. Set the node `spawn.server` to `false` to use the position defined via `/sr spawn`.

## Other infos ##

Contacting me : via Twitter [@Tuetuopay](http://twitter.com/Tuetuopay)